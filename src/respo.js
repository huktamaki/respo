var board = {
  sizeX: null,
  sizeY: null,
}

var colors = {
  background: [255, 255, 128],
  white: [255, 255, 255],
  yellow: [255, 255, 0],
  orange: [255, 128, 0],
  red: [255, 0, 0],
  purple: [128, 0, 128],
  black: [0, 0, 0],
  alpha: 0.4,
  unsolved: 0.35,
}

var texts = {
  font: null,
  large: 128,
  medium: 64,
  small: 24,
  title: 'RESPO',
  menu: ['START', 'ABOUT'],
  selector: '-        -',
  credit: 'AHARONOFF 2020',
  about: 'RESPO IS A PUZZLE VIDEO GAME,\nPLAYING WITH THE IDEA OF\nRESPONSIVENESS.\nTRY TO SOLVE IT BY RESIZING\nTHE WINDOW OF THE BROWSER.',
  grat: 'CONGRATULATION!\n\nYOU HAVE SOLVED IT.\n\nHAVE A NICE DAY!',
}

var states = {
  menu: true,
  item: 0,
  about: false,
  play: false,
  solved: false,
  level: 0,
  grat: false,
}

var sound = {
  bool: true,
  click: null,
  back: null,
  solved: null,
  background : null,
}

function preload() {
  texts.font = loadFont('font/slkscre.ttf')
  board.sizeX = windowWidth
  board.sizeY = windowHeight
  sound.click = loadSound('sound/click.wav')
  sound.back = loadSound('sound/back.wav')
  sound.solved = []
  for (var i = 0; i < 5; i++) {
    sound.solved.push(loadSound('sound/solved' + (i + 1) + '.wav'))
  }
  sound.background = loadSound('sound/background.mp3')
}

function setup() {
  createCanvas(board.sizeX, board.sizeY)
  sound.background.loop()
  sound.background.amp(0.5)
}

function draw() {
  createCanvas(board.sizeX, board.sizeY)
  background(colors.background)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)
  textFont(texts.font)
  textAlign(CENTER)
  // menu
  if (states.menu === true) {
    // title
    textSize(texts.large)
    fill(colors.yellow[0], colors.yellow[1], colors.yellow[2], colors.alpha)
    noStroke()
    text(texts.title, board.sizeX * 0.5, board.sizeY * 0.5 - 100)
    fill(colors.orange[0], colors.orange[1], colors.orange[2], colors.alpha)
    noStroke()
    text(texts.title, board.sizeX * 0.5 + 10, board.sizeY * 0.5 - 100)
    textSize(texts.medium)
    // menu selection
    for (var i = 0; i < texts.menu.length; i++) {
      fill(colors.orange[0], colors.orange[1], colors.orange[2], colors.alpha)
      // selector
      if (mouseX > board.sizeX * 0.5 - (texts.menu[i].length) * 25 && mouseX < board.sizeX * 0.5 + (texts.menu[i].length) * 25 && mouseY > board.sizeY * 0.5 - 10 + i * 80 && mouseY < board.sizeY * 0.5 + 30 + i * 80) {
        text(texts.selector, board.sizeX * 0.5, board.sizeY * 0.5 + 30 + i * 80)
      }
      // items
      textSize(texts.medium)
      text(texts.menu[i], board.sizeX * 0.5, board.sizeY * 0.5 + 30 + i * 80)
    }
    // credit
    fill(colors.red[0], colors.red[1], colors.red[2], colors.alpha)
    textSize(texts.small)
    text(texts.credit, board.sizeX * 0.5, board.sizeY * 0.5 + 200)
  }
  // about
  if (states.about === true) {
    fill(colors.red[0], colors.red[1], colors.red[2], colors.alpha)
    noStroke()
    textSize(texts.small)
    text(texts.about, board.sizeX * 0.5, board.sizeY * 0.5 - 55)
  }
  // play
  if (states.play === true) {
    // level 1 -- white, yellow -- 785 * y
    if (states.level === 0) {
      noStroke()
      // white contains yellow
      if (-board.sizeX * 0.25 + 200 <= 25 && -board.sizeX * 0.25 + 200 >= -25) {
        // white rectangle
        fill(colors.white[0], colors.white[1], colors.white[2], colors.alpha + 0.05 * abs(tan(frameCount * 0.01)))
        rect(board.sizeX * 0.5, board.sizeY * 0.5, 200, 200)
        // yellow rectangle
        fill(colors.yellow[0], colors.yellow[1], colors.yellow[2], colors.alpha + 0.05 * abs(tan(frameCount * 0.01 + 0.05)))
        states.solved = true
      } else {
        // white rectangle
        fill(colors.white[0], colors.white[1], colors.white[2], colors.unsolved)
        rect(board.sizeX * 0.5, board.sizeY * 0.5, 200, 200)
        // yellow rectangle
        fill(colors.yellow[0], colors.yellow[1], colors.yellow[2], colors.unsolved)
        states.solved = false
      }
      rect(board.sizeX * 0.75 - 200, board.sizeY * 0.5, 150, 150)
    }
    // level 2 -- white, yellow, orange -- 450 * 239
    if (states.level === 1) {
      noStroke()
      // white contains yellow
      if (board.sizeY * 0.8 - 200 <= 25 && board.sizeY * 0.8 - 200 >= -25) {
        // yellow contains orange
        if (board.sizeX * 0.25 <= 150 && 125 - board.sizeY * 0.3 <= board.sizeY * 0.5 + 10 - board.sizeX * 0.125 && 275 - board.sizeY * 0.3 >= board.sizeY * 0.5 + 10 + board.sizeX * 0.125 && board.sizeX * 0.5 - 75 <= board.sizeX * 0.5 + 10 - board.sizeX * 0.125 && board.sizeX * 0.5 + 75 >= board.sizeX * 0.5 + 10 + board.sizeX * 0.125) {
          // white rectangle
          fill(colors.white[0], colors.white[1], colors.white[2], colors.alpha + 0.05 * abs(tan(frameCount * 0.01)))
          rect(board.sizeX * 0.5, board.sizeY * 0.5, 200, 200)
          // yellow rectangle
          fill(colors.yellow[0], colors.yellow[1], colors.yellow[2], colors.alpha + 0.05 * abs(tan(frameCount * 0.01 + 0.025)))
          rect(board.sizeX * 0.5, 200 - board.sizeY * 0.3, 150, 150)
          // orange rectangle
          fill(colors.orange[0], colors.orange[1], colors.orange[2], colors.alpha + 0.05 * abs(tan(frameCount * 0.01 + 0.05)))
          states.solved = true
        } else {
          // white rectangle
          fill(colors.white[0], colors.white[1], colors.white[2], colors.unsolved)
          rect(board.sizeX * 0.5, board.sizeY * 0.5, 200, 200)
          // yellow rectangle
          fill(colors.yellow[0], colors.yellow[1], colors.yellow[2], colors.unsolved)
          rect(board.sizeX * 0.5, 200 - board.sizeY * 0.3, 150, 150)
          // orange rectangle
          fill(colors.orange[0], colors.orange[1], colors.orange[2], colors.unsolved)
          states.solved = false
        }
      } else {
        // white rectangle
        fill(colors.white[0], colors.white[1], colors.white[2], colors.unsolved)
        rect(board.sizeX * 0.5, board.sizeY * 0.5, 200, 200)
        // yellow rectangle
        fill(colors.yellow[0], colors.yellow[1], colors.yellow[2], colors.unsolved)
        rect(board.sizeX * 0.5, 200 - board.sizeY * 0.3, 150, 150)
        // orange rectangle
        fill(colors.orange[0], colors.orange[1], colors.orange[2], colors.unsolved)
        states.solved = false
      }
      rect(board.sizeX * 0.5 + 10, board.sizeY * 0.5 + 10, board.sizeX * 0.25, board.sizeX * 0.25)
    }
    // level 3 -- white, yellow, orange, red -- 1128 * 364
    if (states.level === 2) {
      noStroke()
      // white contains yellow
      if (abs(sin(board.sizeX * 0.01)) * 300 >= 250) {
        // yellow contains orange
        if (250 >= abs(tan((board.sizeX + board.sizeY) * 0.01)) * 200 && abs(cos(board.sizeY * 0.01 + 100)) * 250 >= 200) {
          // orange contains red
          if (abs(tan((board.sizeX + board.sizeY) * 0.01) * 200) >= 150 && sin(2 + board.sizeY * 0.005 + board.sizeX * 0.025) * 300 - 75 >= board.sizeY * 0.5 - 100 && sin(2 + board.sizeY * 0.005 + board.sizeX * 0.025) * 300 + 75 <= board.sizeY * 0.5 + 100) {
            // white rectangle
            fill(colors.white[0], colors.white[1], colors.white[2], colors.alpha + 0.05 * abs(tan(frameCount * 0.01)))
            rect(board.sizeX * 0.5, board.sizeY * 0.5, sin(board.sizeX * 0.01) * 300, 300)
            // yellow rectangle
            fill(colors.yellow[0], colors.yellow[1], colors.yellow[2], colors.alpha + 0.05 * abs(tan(frameCount * 0.01 + 0.017)))
            rect(board.sizeX * 0.5, board.sizeY * 0.5, 250, cos(board.sizeY * 0.01 + 100) * 250)
            // orange rectangle
            fill(colors.orange[0], colors.orange[1], colors.orange[2], colors.alpha + 0.05 * abs(tan(frameCount * 0.01 + 0.035)))
            rect(board.sizeX * 0.5, board.sizeY * 0.5, tan((board.sizeX + board.sizeY) * 0.01) * 200, 200)
            // red rectangle
            fill(colors.red[0], colors.red[1], colors.red[2], colors.alpha + 0.05 * abs(tan(frameCount * 0.01 + 0.05)))
            states.solved = true
          } else {
            // white rectangle
            noStroke()
            fill(colors.white[0], colors.white[1], colors.white[2], colors.unsolved)
            rect(board.sizeX * 0.5, board.sizeY * 0.5, sin(board.sizeX * 0.01) * 300, 300)
            // yellow rectangle
            fill(colors.yellow[0], colors.yellow[1], colors.yellow[2], colors.unsolved)
            rect(board.sizeX * 0.5, board.sizeY * 0.5, 250, cos(board.sizeY * 0.01 + 100) * 250)
            // orange rectangle
            fill(colors.orange[0], colors.orange[1], colors.orange[2], colors.unsolved)
            rect(board.sizeX * 0.5, board.sizeY * 0.5, tan((board.sizeX + board.sizeY) * 0.01) * 200, 200)
            // red rectangle
            fill(colors.red[0], colors.red[1], colors.red[2], colors.unsolved)
            states.solved = false
          }
        } else {
          // white rectangle
          noStroke()
          fill(colors.white[0], colors.white[1], colors.white[2], colors.unsolved)
          rect(board.sizeX * 0.5, board.sizeY * 0.5, sin(board.sizeX * 0.01) * 300, 300)
          // yellow rectangle
          fill(colors.yellow[0], colors.yellow[1], colors.yellow[2], colors.unsolved)
          rect(board.sizeX * 0.5, board.sizeY * 0.5, 250, cos(board.sizeY * 0.01 + 100) * 250)
          // orange rectangle
          fill(colors.orange[0], colors.orange[1], colors.orange[2], colors.unsolved)
          rect(board.sizeX * 0.5, board.sizeY * 0.5, tan((board.sizeX + board.sizeY) * 0.01) * 200, 200)
          // red rectangle
          fill(colors.red[0], colors.red[1], colors.red[2], colors.unsolved)
          states.solved = false
        }
      } else {
        // white rectangle
        noStroke()
        fill(colors.white[0], colors.white[1], colors.white[2], colors.unsolved)
        rect(board.sizeX * 0.5, board.sizeY * 0.5, sin(board.sizeX * 0.01) * 300, 300)
        // yellow rectangle
        fill(colors.yellow[0], colors.yellow[1], colors.yellow[2], colors.unsolved)
        rect(board.sizeX * 0.5, board.sizeY * 0.5, 250, cos(board.sizeY * 0.01 + 100) * 250)
        // orange rectangle
        fill(colors.orange[0], colors.orange[1], colors.orange[2], colors.unsolved)
        rect(board.sizeX * 0.5, board.sizeY * 0.5, tan((board.sizeX + board.sizeY) * 0.01) * 200, 200)
        // red rectangle
        fill(colors.red[0], colors.red[1], colors.red[2], colors.unsolved)
        states.solved = false
      }
      rect(board.sizeX * 0.5, sin(2 + board.sizeY * 0.005 + board.sizeX * 0.025) * 300, 150, 150)
    }
    // level 4 -- white, yellow, orange, red, purple -- 1078 * 413
    if (states.level === 3) {
      noStroke()
      // white contains yellow
      if (abs(300 - 29 * (board.sizeX % 23)) >= 250 && board.sizeX * 0.5 - 0.5 * 30 * (board.sizeX % 30) <= board.sizeX * 0.5 + 20 * (board.sizeY % 31) - 125 && board.sizeX * 0.5 + 0.5 * 30 * (board.sizeX % 30) >= board.sizeX * 0.5 + 20 * (board.sizeY % 31) + 125) {
        // yellow contains orange
        if (board.sizeX * 0.5 + 20 * (board.sizeY % 31) - 0.5 * 250 <= board.sizeX * 0.5 + 190 * abs(sin(board.sizeX * 0.01)) - 0.5 * 200 && board.sizeX * 0.5 + 20 * (board.sizeY % 31) + 0.5 * 250 >= board.sizeX * 0.5 + 190 * abs(sin(board.sizeX * 0.01)) + 0.5 * 200) {
          // orange contains red
          if (board.sizeX * 0.5 + 190 * abs(sin(board.sizeX * 0.01)) - 0.5 * 200 <= board.sizeX * 0.5 + (120 - board.sizeX * 0.005) - 0.5 * 150 * abs(cos(board.sizeX * 0.01 + board.sizeY * 0.1)) && board.sizeX * 0.5 + 190 * abs(sin(board.sizeX * 0.01)) + 0.5 * 200 >= board.sizeX * 0.5 + (120 - board.sizeX * 0.005) + 0.5 * 150 * abs(cos(board.sizeX * 0.01 + board.sizeY * 0.1)) && board.sizeY * 0.5 - 0.5 * 200 <= board.sizeY * 0.5 + 30 - 0.5 * 150 * abs(tan((board.sizeY % 200) * 0.01)) && board.sizeY * 0.5 + 0.5 * 200 >= board.sizeY * 0.5 + 30 + 0.5 * 150 * abs(tan((board.sizeY % 200) * 0.01))) {
            // red contains purple
            if (board.sizeX * 0.5 + (120 - board.sizeX * 0.005) - 0.5 * 150 * abs(cos(board.sizeX * 0.01 + board.sizeY * 0.1)) <= board.sizeX * 0.5 + 100 - 200 * cos(board.sizeY * 0.0192) - 0.5 * 40 * abs(sin(board.sizeX / 48)) && board.sizeX * 0.5 + (120 - board.sizeX * 0.005) + 0.5 * 150 * abs(cos(board.sizeX * 0.01 + board.sizeY * 0.1)) >= board.sizeX * 0.5 + 100 - 200 * cos(board.sizeY * 0.0192) + 0.5 * 40 * abs(sin(board.sizeX / 48)) && board.sizeY * 0.5 + 30 - 0.5 * 150 * abs(tan((board.sizeY % 200) * 0.01)) <= board.sizeY * 0.5 + 30 * tan(board.sizeY * 0.04) - 0.5 * 10 * ceil((board.sizeX % 43) / 37) && board.sizeY * 0.5 + 30 + 0.5 * 150 * abs(tan((board.sizeY % 200) * 0.01)) >= board.sizeY * 0.5 + 30 * tan(board.sizeY * 0.04) + 0.5 * 10 * ceil((board.sizeX % 43) / 37)) {
              // white rectangle
              fill(colors.white[0], colors.white[1], colors.white[2], colors.alpha + 0.05 * abs(tan(frameCount * 0.01)))
              rect(board.sizeX * 0.5, board.sizeY * 0.5, 30 * (board.sizeX % 30), abs(300 - 29 * (board.sizeX % 23)))
              // yellow rectangle
              fill(colors.yellow[0], colors.yellow[1], colors.yellow[2], colors.alpha + 0.05 * abs(tan(frameCount * 0.01 + 0.0125)))
              rect(board.sizeX * 0.5 + 20 * (board.sizeY % 31), board.sizeY * 0.5, 250, 250)
              // orange rectangle
              fill(colors.orange[0], colors.orange[1], colors.orange[2], colors.alpha + 0.05 * abs(tan(frameCount * 0.01 + 0.025)))
              rect(board.sizeX * 0.5 + 190 * abs(sin(board.sizeX * 0.01)), board.sizeY * 0.5, 200, 200)
              // red rectangle
              fill(colors.red[0], colors.red[1], colors.red[2], colors.alpha + 0.05 * abs(tan(frameCount * 0.01 + 0.0375)))
              rect(board.sizeX * 0.5 + (120 - board.sizeX * 0.005), board.sizeY * 0.5 + 30, 150 * abs(cos(board.sizeX * 0.01 + board.sizeY * 0.1)), 150 * abs(tan((board.sizeY % 200) * 0.01)))
              // purple rectangle
              fill(colors.purple[0], colors.purple[1], colors.purple[2], colors.alpha + 0.05 * abs(tan(frameCount * 0.01 + 0.05)))
              states.solved = true
            } else {
              // white rectangle
              fill(colors.white[0], colors.white[1], colors.white[2], colors.unsolved)
              rect(board.sizeX * 0.5, board.sizeY * 0.5, 30 * (board.sizeX % 30), abs(300 - 29 * (board.sizeX % 23)))
              // yellow rectangle
              fill(colors.yellow[0], colors.yellow[1], colors.yellow[2], colors.unsolved)
              rect(board.sizeX * 0.5 + 20 * (board.sizeY % 31), board.sizeY * 0.5, 250, 250)
              // orange rectangle
              fill(colors.orange[0], colors.orange[1], colors.orange[2], colors.unsolved)
              rect(board.sizeX * 0.5 + 190 * abs(sin(board.sizeX * 0.01)), board.sizeY * 0.5, 200, 200)
              // red rectangle
              fill(colors.red[0], colors.red[1], colors.red[2], colors.unsolved)
              rect(board.sizeX * 0.5 + (120 - board.sizeX * 0.005), board.sizeY * 0.5 + 30, 150 * abs(cos(board.sizeX * 0.01 + board.sizeY * 0.1)), 150 * abs(tan((board.sizeY % 200) * 0.01)))
              // purple rectangle
              fill(colors.purple[0], colors.purple[1], colors.purple[2], colors.unsolved)
              states.solved = false
            }
          } else {
            // white rectangle
            fill(colors.white[0], colors.white[1], colors.white[2], colors.unsolved)
            rect(board.sizeX * 0.5, board.sizeY * 0.5, 30 * (board.sizeX % 30), abs(300 - 29 * (board.sizeX % 23)))
            // yellow rectangle
            fill(colors.yellow[0], colors.yellow[1], colors.yellow[2], colors.unsolved)
            rect(board.sizeX * 0.5 + 20 * (board.sizeY % 31), board.sizeY * 0.5, 250, 250)
            // orange rectangle
            fill(colors.orange[0], colors.orange[1], colors.orange[2], colors.unsolved)
            rect(board.sizeX * 0.5 + 190 * abs(sin(board.sizeX * 0.01)), board.sizeY * 0.5, 200, 200)
            // red rectangle
            fill(colors.red[0], colors.red[1], colors.red[2], colors.unsolved)
            rect(board.sizeX * 0.5 + (120 - board.sizeX * 0.005), board.sizeY * 0.5 + 30, 150 * abs(cos(board.sizeX * 0.01 + board.sizeY * 0.1)), 150 * abs(tan((board.sizeY % 200) * 0.01)))
            // purple rectangle
            fill(colors.purple[0], colors.purple[1], colors.purple[2], colors.unsolved)
            states.solved = false
          }
        } else {
          // white rectangle
          fill(colors.white[0], colors.white[1], colors.white[2], colors.unsolved)
          rect(board.sizeX * 0.5, board.sizeY * 0.5, 30 * (board.sizeX % 30), abs(300 - 29 * (board.sizeX % 23)))
          // yellow rectangle
          fill(colors.yellow[0], colors.yellow[1], colors.yellow[2], colors.unsolved)
          rect(board.sizeX * 0.5 + 20 * (board.sizeY % 31), board.sizeY * 0.5, 250, 250)
          // orange rectangle
          fill(colors.orange[0], colors.orange[1], colors.orange[2], colors.unsolved)
          rect(board.sizeX * 0.5 + 190 * abs(sin(board.sizeX * 0.01)), board.sizeY * 0.5, 200, 200)
          // red rectangle
          fill(colors.red[0], colors.red[1], colors.red[2], colors.unsolved)
          rect(board.sizeX * 0.5 + (120 - board.sizeX * 0.005), board.sizeY * 0.5 + 30, 150 * abs(cos(board.sizeX * 0.01 + board.sizeY * 0.1)), 150 * abs(tan((board.sizeY % 200) * 0.01)))
          // purple rectangle
          fill(colors.purple[0], colors.purple[1], colors.purple[2], colors.unsolved)
          states.solved = false
        }
      } else {
        // white rectangle
        fill(colors.white[0], colors.white[1], colors.white[2], colors.unsolved)
        rect(board.sizeX * 0.5, board.sizeY * 0.5, 30 * (board.sizeX % 30), abs(300 - 29 * (board.sizeX % 23)))
        // yellow rectangle
        fill(colors.yellow[0], colors.yellow[1], colors.yellow[2], colors.unsolved)
        rect(board.sizeX * 0.5 + 20 * (board.sizeY % 31), board.sizeY * 0.5, 250, 250)
        // orange rectangle
        fill(colors.orange[0], colors.orange[1], colors.orange[2], colors.unsolved)
        rect(board.sizeX * 0.5 + 190 * abs(sin(board.sizeX * 0.01)), board.sizeY * 0.5, 200, 200)
        // red rectangle
        fill(colors.red[0], colors.red[1], colors.red[2], colors.unsolved)
        rect(board.sizeX * 0.5 + (120 - board.sizeX * 0.005), board.sizeY * 0.5 + 30, 150 * abs(cos(board.sizeX * 0.01 + board.sizeY * 0.1)), 150 * abs(tan((board.sizeY % 200) * 0.01)))
        // purple rectangle
        fill(colors.purple[0], colors.purple[1], colors.purple[2], colors.unsolved)
        states.solved = false
      }
      rect(board.sizeX * 0.5 + 100 - 200 * cos(board.sizeY * 0.0192), board.sizeY * 0.5 + 30 * tan(board.sizeY * 0.04), 40 * abs(sin(board.sizeX / 48)), 10 * ceil((board.sizeX % 43) / 37))
    }
    // level 5 -- white, yellow, orange, red, purple, black -- 911 * 457
    if (states.level === 4) {
      noStroke()
      // white contains yellow
      if (board.sizeX * 0.5 - 25 + 100 * sin(board.sizeX * 0.01) - 0.5 * 300 <= board.sizeX * 0.5 - 0.5 * 250 && board.sizeX * 0.5 - 25 + 100 * sin(board.sizeX * 0.01) + 0.5 * 300 >= board.sizeX * 0.5 + 0.5 * 250 && board.sizeY * 0.5 - 0.5 * 600 * abs(cos(board.sizeY * 0.05)) <= board.sizeY * 0.5 + 300 * abs(tan(sin(board.sizeX * 0.1))) - 0.5 * 250 && board.sizeY * 0.5 + 0.5 * 600 * abs(cos(board.sizeY * 0.05)) >= board.sizeY * 0.5 + 300 * abs(tan(sin(board.sizeX * 0.1))) + 0.5 * 250) {
        // yellow contains orange
        if (board.sizeX * 0.5 - 0.5 * 250 <= board.sizeX * 0.5 - 200 + 400 * sin(board.sizeX * 0.01 + board.sizeY * 0.05) - 0.5 * 200 * pow(abs(1 + sin(board.sizeX * 0.1)), board.sizeX % 2) && board.sizeX * 0.5 + 0.5 * 250 >= board.sizeX * 0.5 - 200 + 400 * sin(board.sizeX * 0.01 + board.sizeY * 0.05) + 0.5 * 200 * pow(abs(1 + sin(board.sizeX * 0.1)), board.sizeX % 2) && board.sizeY * 0.5 + 300 * abs(tan(sin(board.sizeX * 0.1))) - 0.5 * 250 <= board.sizeY * 0.5 - 0.5 * 200 && board.sizeY * 0.5 + 300 * abs(tan(sin(board.sizeX * 0.1))) + 0.5 * 250 >= board.sizeY * 0.5 + 0.5 * 200) {
          // orange contains red
          if (board.sizeX * 0.5 - 200 + 400 * sin(board.sizeX * 0.01 + board.sizeY * 0.05) - 0.5 * 200 * pow(abs(1 + sin(board.sizeX * 0.1)), board.sizeX % 2) <= board.sizeX * 0.5 + 400 - 200 * max(board.sizeX % 5, board.sizeY % 5) - 0.5 * 200 * abs(tan(board.sizeX * 0.02)) && board.sizeX * 0.5 - 200 + 400 * sin(board.sizeX * 0.01 + board.sizeY * 0.05) + 0.5 * 200 * pow(abs(1 + sin(board.sizeX * 0.1)), board.sizeX % 2) >= board.sizeX * 0.5 + 400 - 200 * max(board.sizeX % 5, board.sizeY % 5) + 0.5 * 200 * abs(tan(board.sizeX * 0.02)) && board.sizeY * 0.5 - 0.5 * 200 <= board.sizeY * 0.5 - 0.5 * 150 && board.sizeY * 0.5 + 0.5 * 200 >= board.sizeY * 0.5 + 0.5 * 150) {
            // red contains purple
            if (board.sizeX * 0.5 + 400 - 200 * max(board.sizeX % 5, board.sizeY % 5) - 0.5 * 200 * abs(tan(board.sizeX * 0.02)) <= board.sizeX * 0.5 + 300 * cos(board.sizeY * 0.0035) - 0.5 * 200 * abs(tan((board.sizeX - board.sizeY)) * 0.02) && board.sizeX * 0.5 + 400 - 200 * max(board.sizeX % 5, board.sizeY % 5) + 0.5 * 200 * abs(tan(board.sizeX * 0.02)) >= board.sizeX * 0.5 + 300 * cos(board.sizeY * 0.0035) + 0.5 * 200 * abs(tan((board.sizeX - board.sizeY)) * 0.02) && board.sizeY * 0.5 - 0.5 * 150 <= board.sizeY * 0.5 + 200 * sq(sin(board.sizeX * 0.1)) - 0.5 * 100 && board.sizeY * 0.5 + 0.5 * 150 >= board.sizeY * 0.5 + 200 * sq(sin(board.sizeX * 0.1)) + 0.5 * 100) {
              // purple contains black
              if (board.sizeX * 0.5 + 300 * cos(board.sizeY * 0.0035) - 0.5 * 200 * abs(tan((board.sizeX - board.sizeY)) * 0.02) <= board.sizeX * 0.5 - 300 * sin(board.sizeX * 0.1) - 0.5 * 480 * abs(sqrt(abs(sin(board.sizeY * 0.01))) - 0.85) && board.sizeX * 0.5 + 300 * cos(board.sizeY * 0.0035) + 0.5 * 200 * abs(tan((board.sizeX - board.sizeY)) * 0.02) >= board.sizeX * 0.5 - 300 * sin(board.sizeX * 0.1) + 0.5 * 480 * abs(sqrt(abs(sin(board.sizeY * 0.01))) - 0.85) && board.sizeY * 0.5 + 200 * sq(sin(board.sizeX * 0.1)) - 0.5 * 100 <= board.sizeY * 0.5 + 200 * cos(board.sizeX * 0.05) - 0.5 * 210 * exp(abs(cos(board.sizeX * 0.01) + 1) - 1.25) && board.sizeY * 0.5 + 200 * sq(sin(board.sizeX * 0.1)) + 0.5 * 100 >= board.sizeY * 0.5 + 200 * cos(board.sizeX * 0.05) + 0.5 * 210 * exp(abs(cos(board.sizeX * 0.01) + 1) - 1.25)) {
                // white rectangle
                fill(colors.white[0], colors.white[1], colors.white[2], colors.alpha)
                rect(board.sizeX * 0.5 - 25 + 100 * sin(board.sizeX * 0.01), board.sizeY * 0.5, 300, 600 * abs(cos(board.sizeY * 0.05)))
                // yellow rectangle
                fill(colors.yellow[0], colors.yellow[1], colors.yellow[2], colors.alpha + 0.05 * abs(tan(frameCount * 0.01 + 0.01)))
                rect(board.sizeX * 0.5, board.sizeY * 0.5 + 300 * abs(tan(sin(board.sizeX * 0.1))), 250, 250)
                // orange rectangle
                fill(colors.orange[0], colors.orange[1], colors.orange[2], colors.alpha + 0.05 * abs(tan(frameCount * 0.01 + 0.02)))
                rect(board.sizeX * 0.5 - 200 + 400 * sin(board.sizeX * 0.01 + board.sizeY * 0.05), board.sizeY * 0.5, 200 * pow(abs(1 + sin(board.sizeX * 0.1)), board.sizeX % 2), 200)
                // red rectangle
                fill(colors.red[0], colors.red[1], colors.red[2], colors.alpha + 0.05 * abs(tan(frameCount * 0.01 + 0.03)))
                rect(board.sizeX * 0.5 + 400 - 200 * max(board.sizeX % 5, board.sizeY % 5), board.sizeY * 0.5, 200 * abs(tan(board.sizeX * 0.02)), 150)
                // purple rectangle
                fill(colors.purple[0], colors.purple[1], colors.purple[2], colors.alpha + 0.05 * abs(tan(frameCount * 0.01 + 0.04)))
                rect(board.sizeX * 0.5 + 300 * cos(board.sizeY * 0.0035), board.sizeY * 0.5 + 200 * sq(sin(board.sizeX * 0.1)), 200 * abs(tan((board.sizeX - board.sizeY)) * 0.02), 100)
                // black rectangle
                fill(colors.black[0], colors.black[1], colors.black[2], colors.alpha + 0.05 * abs(tan(frameCount * 0.01 + 0.05)))
                states.solved = true
              } else {
                // white rectangle
                fill(colors.white[0], colors.white[1], colors.white[2], colors.unsolved)
                rect(board.sizeX * 0.5 - 25 + 100 * sin(board.sizeX * 0.01), board.sizeY * 0.5, 300, 600 * abs(cos(board.sizeY * 0.05)))
                // yellow rectangle
                fill(colors.yellow[0], colors.yellow[1], colors.yellow[2], colors.unsolved)
                rect(board.sizeX * 0.5, board.sizeY * 0.5 + 300 * abs(tan(sin(board.sizeX * 0.1))), 250, 250)
                // orange rectangle
                fill(colors.orange[0], colors.orange[1], colors.orange[2], colors.unsolved)
                rect(board.sizeX * 0.5 - 200 + 400 * sin(board.sizeX * 0.01 + board.sizeY * 0.05), board.sizeY * 0.5, 200 * pow(abs(1 + sin(board.sizeX * 0.1)), board.sizeX % 2), 200)
                // red rectangle
                fill(colors.red[0], colors.red[1], colors.red[2], colors.unsolved)
                rect(board.sizeX * 0.5 + 400 - 200 * max(board.sizeX % 5, board.sizeY % 5), board.sizeY * 0.5, 200 * abs(tan(board.sizeX * 0.02)), 150)
                // purple rectangle
                fill(colors.purple[0], colors.purple[1], colors.purple[2], colors.unsolved)
                rect(board.sizeX * 0.5 + 300 * cos(board.sizeY * 0.0035), board.sizeY * 0.5 + 200 * sq(sin(board.sizeX * 0.1)), 200 * abs(tan((board.sizeX - board.sizeY)) * 0.02), 100)
                // black rectangle
                fill(colors.black[0], colors.black[1], colors.black[2], colors.unsolved)
                states.solved = false
              }
            } else {
              // white rectangle
              fill(colors.white[0], colors.white[1], colors.white[2], colors.unsolved)
              rect(board.sizeX * 0.5 - 25 + 100 * sin(board.sizeX * 0.01), board.sizeY * 0.5, 300, 600 * abs(cos(board.sizeY * 0.05)))
              // yellow rectangle
              fill(colors.yellow[0], colors.yellow[1], colors.yellow[2], colors.unsolved)
              rect(board.sizeX * 0.5, board.sizeY * 0.5 + 300 * abs(tan(sin(board.sizeX * 0.1))), 250, 250)
              // orange rectangle
              fill(colors.orange[0], colors.orange[1], colors.orange[2], colors.unsolved)
              rect(board.sizeX * 0.5 - 200 + 400 * sin(board.sizeX * 0.01 + board.sizeY * 0.05), board.sizeY * 0.5, 200 * pow(abs(1 + sin(board.sizeX * 0.1)), board.sizeX % 2), 200)
              // red rectangle
              fill(colors.red[0], colors.red[1], colors.red[2], colors.unsolved)
              rect(board.sizeX * 0.5 + 400 - 200 * max(board.sizeX % 5, board.sizeY % 5), board.sizeY * 0.5, 200 * abs(tan(board.sizeX * 0.02)), 150)
              // purple rectangle
              fill(colors.purple[0], colors.purple[1], colors.purple[2], colors.unsolved)
              rect(board.sizeX * 0.5 + 300 * cos(board.sizeY * 0.0035), board.sizeY * 0.5 + 200 * sq(sin(board.sizeX * 0.1)), 200 * abs(tan((board.sizeX - board.sizeY)) * 0.02), 100)
              // black rectangle
              fill(colors.black[0], colors.black[1], colors.black[2], colors.unsolved)
              states.solved = false
            }
          } else {
            // white rectangle
            fill(colors.white[0], colors.white[1], colors.white[2], colors.unsolved)
            rect(board.sizeX * 0.5 - 25 + 100 * sin(board.sizeX * 0.01), board.sizeY * 0.5, 300, 600 * abs(cos(board.sizeY * 0.05)))
            // yellow rectangle
            fill(colors.yellow[0], colors.yellow[1], colors.yellow[2], colors.unsolved)
            rect(board.sizeX * 0.5, board.sizeY * 0.5 + 300 * abs(tan(sin(board.sizeX * 0.1))), 250, 250)
            // orange rectangle
            fill(colors.orange[0], colors.orange[1], colors.orange[2], colors.unsolved)
            rect(board.sizeX * 0.5 - 200 + 400 * sin(board.sizeX * 0.01 + board.sizeY * 0.05), board.sizeY * 0.5, 200 * pow(abs(1 + sin(board.sizeX * 0.1)), board.sizeX % 2), 200)
            // red rectangle
            fill(colors.red[0], colors.red[1], colors.red[2], colors.unsolved)
            rect(board.sizeX * 0.5 + 400 - 200 * max(board.sizeX % 5, board.sizeY % 5), board.sizeY * 0.5, 200 * abs(tan(board.sizeX * 0.02)), 150)
            // purple rectangle
            fill(colors.purple[0], colors.purple[1], colors.purple[2], colors.unsolved)
            rect(board.sizeX * 0.5 + 300 * cos(board.sizeY * 0.0035), board.sizeY * 0.5 + 200 * sq(sin(board.sizeX * 0.1)), 200 * abs(tan((board.sizeX - board.sizeY)) * 0.02), 100)
            // black rectangle
            fill(colors.black[0], colors.black[1], colors.black[2], colors.unsolved)
            states.solved = false
          }
        } else {
          // white rectangle
          fill(colors.white[0], colors.white[1], colors.white[2], colors.unsolved)
          rect(board.sizeX * 0.5 - 25 + 100 * sin(board.sizeX * 0.01), board.sizeY * 0.5, 300, 600 * abs(cos(board.sizeY * 0.05)))
          // yellow rectangle
          fill(colors.yellow[0], colors.yellow[1], colors.yellow[2], colors.unsolved)
          rect(board.sizeX * 0.5, board.sizeY * 0.5 + 300 * abs(tan(sin(board.sizeX * 0.1))), 250, 250)
          // orange rectangle
          fill(colors.orange[0], colors.orange[1], colors.orange[2], colors.unsolved)
          rect(board.sizeX * 0.5 - 200 + 400 * sin(board.sizeX * 0.01 + board.sizeY * 0.05), board.sizeY * 0.5, 200 * pow(abs(1 + sin(board.sizeX * 0.1)), board.sizeX % 2), 200)
          // red rectangle
          fill(colors.red[0], colors.red[1], colors.red[2], colors.unsolved)
          rect(board.sizeX * 0.5 + 400 - 200 * max(board.sizeX % 5, board.sizeY % 5), board.sizeY * 0.5, 200 * abs(tan(board.sizeX * 0.02)), 150)
          // purple rectangle
          fill(colors.purple[0], colors.purple[1], colors.purple[2], colors.unsolved)
          rect(board.sizeX * 0.5 + 300 * cos(board.sizeY * 0.0035), board.sizeY * 0.5 + 200 * sq(sin(board.sizeX * 0.1)), 200 * abs(tan((board.sizeX - board.sizeY)) * 0.02), 100)
          // black rectangle
          fill(colors.black[0], colors.black[1], colors.black[2], colors.unsolved)
          states.solved = false
        }
      } else {
        // white rectangle
        fill(colors.white[0], colors.white[1], colors.white[2], colors.unsolved)
        rect(board.sizeX * 0.5 - 25 + 100 * sin(board.sizeX * 0.01), board.sizeY * 0.5, 300, 600 * abs(cos(board.sizeY * 0.05)))
        // yellow rectangle
        fill(colors.yellow[0], colors.yellow[1], colors.yellow[2], colors.unsolved)
        rect(board.sizeX * 0.5, board.sizeY * 0.5 + 300 * abs(tan(sin(board.sizeX * 0.1))), 250, 250)
        // orange rectangle
        fill(colors.orange[0], colors.orange[1], colors.orange[2], colors.unsolved)
        rect(board.sizeX * 0.5 - 200 + 400 * sin(board.sizeX * 0.01 + board.sizeY * 0.05), board.sizeY * 0.5, 200 * pow(abs(1 + sin(board.sizeX * 0.1)), board.sizeX % 2), 200)
        // red rectangle
        fill(colors.red[0], colors.red[1], colors.red[2], colors.unsolved)
        rect(board.sizeX * 0.5 + 400 - 200 * max(board.sizeX % 5, board.sizeY % 5), board.sizeY * 0.5, 200 * abs(tan(board.sizeX * 0.02)), 150)
        // purple rectangle
        fill(colors.purple[0], colors.purple[1], colors.purple[2], colors.unsolved)
        rect(board.sizeX * 0.5 + 300 * cos(board.sizeY * 0.0035), board.sizeY * 0.5 + 200 * sq(sin(board.sizeX * 0.1)), 200 * abs(tan((board.sizeX - board.sizeY)) * 0.02), 100)
        // black rectangle
        fill(colors.black[0], colors.black[1], colors.black[2], colors.unsolved)
        states.solved = false
      }
      rect(board.sizeX * 0.5 - 300 * sin(board.sizeX * 0.1), board.sizeY * 0.5 + 200 * cos(board.sizeX * 0.05), 480 * abs(sqrt(abs(sin(board.sizeY * 0.01))) - 0.85), 210 * exp(abs(cos(board.sizeX * 0.01) + 1) - 1.25))
    }
  }
  // congratulation
  if (states.grat === true) {
    fill(colors.red[0], colors.red[1], colors.red[2], colors.alpha)
    noStroke()
    textSize(texts.small)
    text(texts.grat, board.sizeX * 0.5, board.sizeY * 0.5 - 55)
  }
}

function mousePressed() {
  // menu
  if (states.menu === true) {
    // enter play
    if (mouseX > board.sizeX * 0.5 - texts.menu[0].length * 25 && mouseX < board.sizeX * 0.5 + texts.menu[0].length * 25 && mouseY > board.sizeY * 0.5 - 10 && mouseY < board.sizeY * 0.5 + 30) {
      setTimeout(function() {
        states.menu = false
        states.play = true
        states.level = 0
      })
      playSound(sound.click)
    }
    // enter about
    if (mouseX > board.sizeX * 0.5 - texts.menu[1].length * 25 && mouseX < board.sizeX * 0.5 + texts.menu[1].length * 25 && mouseY > board.sizeY * 0.5 + 70 && mouseY < board.sizeY * 0.5 + 110) {
      setTimeout(function() {
        states.menu = false
        states.about = true
      })
      playSound(sound.click)
    }
  }
  // about
  if (states.about === true) {
    states.menu = true
    states.about = false
    playSound(sound.back)
  }
  // play
  if (states.play === true) {
    // level 1
    if (states.level === 0) {
      if (states.solved === true) {
        playSound(sound.solved[floor(random() * 5)])
        if (mouseX > board.sizeX * 0.75 - 275 && mouseX < board.sizeX * 0.75 - 125) {
          states.level++
          states.solved = false
        }
      }
    }
    // level 2
    if (states.level === 1) {
      if (states.solved === true) {
        playSound(sound.solved[floor(random() * 5)])
        if (mouseX > board.sizeX * 0.5 + 10 - board.sizeX * 0.125 && mouseX < board.sizeX * 0.5 + 10 + board.sizeX * 0.125 && mouseY > board.sizeY * 0.5 + 10 - board.sizeX * 0.125 && mouseY < board.sizeY * 0.5 + 10 + board.sizeX * 0.125) {
          states.level++
          states.solved = false
        }
      }
    }
    // level 3
    if (states.level === 2) {
      if (states.solved === true) {
        playSound(sound.solved[floor(random() * 5)])
        if (mouseX > board.sizeX * 0.5 - 75 && mouseX < board.sizeX * 0.5 + 75 && mouseY > sin(2 + board.sizeY * 0.005 + board.sizeX * 0.025) * 300 - 75 && mouseY < sin(2 + board.sizeY * 0.005 + board.sizeX * 0.025) * 300 + 75) {
          states.level++
          states.solved = false
        }
      }
    }
    // level 4
    if (states.level === 3) {
      if (states.solved === true) {
        playSound(sound.solved[floor(random() * 5)])
        if (mouseX > board.sizeX * 0.5 + 100 - 200 * cos(board.sizeY * 0.0192) - 0.5 * 40 * abs(sin(board.sizeX / 48)) && mouseX < board.sizeX * 0.5 + 100 - 200 * cos(board.sizeY * 0.0192) + 0.5 * 40 * abs(sin(board.sizeX / 48)) && mouseY > board.sizeY * 0.5 + 30 * tan(board.sizeY * 0.04) - 0.5 * 10 * ceil((board.sizeX % 43) / 37) && mouseY < board.sizeY * 0.5 + 30 * tan(board.sizeY * 0.04) + 0.5 * 10 * ceil((board.sizeX % 43) / 37)) {
          states.level++
          states.solved = false
        }
      }
    }
    // level 5
    if (states.level === 4) {
      if (states.solved === true) {
        playSound(sound.solved[floor(random() * 5)])
        if (mouseX > board.sizeX * 0.5 - 300 * sin(board.sizeX * 0.1) - 0.5 * 480 * abs(sqrt(abs(sin(board.sizeY * 0.01))) - 0.85) && mouseX < board.sizeX * 0.5 - 300 * sin(board.sizeX * 0.1) + 0.5 * 480 * abs(sqrt(abs(sin(board.sizeY * 0.01))) - 0.85) && mouseY > board.sizeY * 0.5 + 200 * cos(board.sizeX * 0.05) - 0.5 * 210 * exp(abs(cos(board.sizeX * 0.01) + 1) - 1.25) && mouseY < board.sizeY * 0.5 + 200 * cos(board.sizeX * 0.05) + 0.5 * 210 * exp(abs(cos(board.sizeX * 0.01) + 1) - 1.25)) {
          setTimeout(function() {
            states.solved = false
            states.play = false
            states.grat = true
          }, 1)
        }
      }
    }
  }
  // congratulation
  if (states.grat === true) {
    states.menu = true
    states.grat = false
    playSound(sound.back)
  }
}

function playSound(s) {
  if (sound.bool === true) {
    s.play()
  }
}

function windowResized() {
  board.sizeX = windowWidth
  board.sizeY = windowHeight
}
